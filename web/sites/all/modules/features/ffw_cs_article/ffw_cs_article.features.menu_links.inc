<?php
/**
 * @file
 * ffw_cs_article.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ffw_cs_article_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_ffw-cs-article:ffw-cs-article.
  $menu_links['main-menu_ffw-cs-article:ffw-cs-article'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'ffw-cs-article',
    'router_path' => 'ffw-cs-article',
    'link_title' => 'FFW CS Article',
    'options' => array(
      'identifier' => 'main-menu_ffw-cs-article:ffw-cs-article',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_news:ffw-cs-article.
  $menu_links['main-menu_news:ffw-cs-article'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'ffw-cs-article',
    'router_path' => 'ffw-cs-article',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => 'My first content type',
      ),
      'identifier' => 'main-menu_news:ffw-cs-article',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('FFW CS Article');
  t('News');

  return $menu_links;
}
