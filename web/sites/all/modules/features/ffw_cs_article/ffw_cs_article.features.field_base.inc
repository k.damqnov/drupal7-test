<?php
/**
 * @file
 * ffw_cs_article.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ffw_cs_article_field_default_field_bases() {
  $field_bases = [];

  //some code to test phpstorm

  // Exported field_base: 'body'.
  $field_bases['body'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [
      0 => 'node',
    ],
    'field_name' => 'body',
    'indexes' => [
      'format' => [
        0 => 'format',
      ],
    ],
    'locked' => 0,
    'module' => 'text',
    'settings' => [],
    'translatable' => 0,
    'type' => 'text_with_summary',
  ];

  // Exported field_base: 'field_ffw_cs_article_image'.
  $field_bases['field_ffw_cs_article_image'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'field_ffw_cs_article_image',
    'indexes' => [
      'fid' => [
        0 => 'fid',
      ],
    ],
    'locked' => 0,
    'module' => 'image',
    'settings' => [
      'default_image' => 1,
      'uri_scheme' => 'public',
    ],
    'translatable' => 0,
    'type' => 'image',
  ];

  return $field_bases;
}
